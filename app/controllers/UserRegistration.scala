package controllers

import com.google.inject.Key
import com.google.inject.name.Names
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import net.tanesha.recaptcha.ReCaptcha
import play.api.data.{Form, Forms}
import play.api.data.validation.Constraints
import play.api.mvc.{Action, Controller}
import topfeed.server.{UsernameIsTakenException, UsersService}
import topfeed.server.config.SimpleConstants
import topfeed.server.config.di.InjectorProvider
import topfeed.server.domain.User
import topfeed.server.security.HttpsOnlyActionCompositor
import topfeed.server.utils.Pbkdf2Password

object UserRegistration extends Controller with InjectorProvider with SimpleConstants
with HttpsOnlyActionCompositor {	// TODO: SimpleConstants seems to be unused.
  // TODO: Setup constructor injection with controllers.
  lazy val usersService = injector.instance[UsersService]
  lazy val recaptcha = injector.instance[ReCaptcha]
  lazy val recaptchaPK = injector.getInstance(Key.get(classOf[String], Names.named("RecaptchaPublicKey")))
  
  private def registerationViewWith(form: Form[RegistrationForm]) =
    views.html.registerUser(form, recaptchaPK)
  
  def showForm = httpsOnly{ Action {
      Ok(registerationViewWith(emptyRegistrationForm))
  }}
  
  def processForm = httpsOnly{ Action {implicit request =>
    emptyRegistrationForm.bindFromRequest.fold (
      formWithErrors => BadRequest(registerationViewWith(formWithErrors)),
      form => 
        if(! recaptcha.checkAnswer(request.remoteAddress, form.captchaChallenge, form.captchaResponse).isValid)
          BadRequest(registerationViewWith(emptyRegistrationForm.fill(form).withGlobalError("The captcha value entered was invalid!")))
        else try {
          usersService.addUser(User(form.username, Pbkdf2Password.hashOf(form.password)))
          Ok("You have registered successfully!\n\nNow, close this window and log-in.")
        } catch {
          case e: UsernameIsTakenException =>
            BadRequest(registerationViewWith(emptyRegistrationForm.fill(form).withError("username", "That username is taken!")))
        }
    )
  }}
  
  case class RegistrationForm(username: String, password: String, passwordConfirm:String, captchaChallenge: String, captchaResponse: String)
  
  private def emptyRegistrationForm = Form (
	Forms.mapping(
	  "username" -> Forms.text.verifying(Constraints.pattern(usernameRegexString.r, "", usernameValidationDescription)),
      "password" -> Forms.text(minLength = 6),
      "password-confirm" -> Forms.text(minLength = 6),
      "recaptcha_challenge_field" -> Forms.text,
      "recaptcha_response_field"  -> Forms.text
	)(RegistrationForm.apply)(RegistrationForm.unapply)
	.verifying("The passwords didn't match!", form => form.password == form.passwordConfirm)
	.verifying("The captcha was left empty!", _.captchaResponse != "")
  )
  
  def usernameRegexString = """^[a-zA-Z][a-zA-Z0-9_]{1,19}$"""
  def usernameValidationDescription = "Only alpha-numeric letters and underscore are allowed."
}
