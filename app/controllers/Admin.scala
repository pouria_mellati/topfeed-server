package controllers

import play.api._
import play.api.mvc._
import topfeed.server.config.di.InjectorProvider
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import org.neo4j.scala.DatabaseService
import org.neo4j.cypher.ExecutionEngine
import org.neo4j.cypher.SyntaxException
import play.api.templates.Html
import topfeed.server.security.{AdminAction, HttpsOnlyActionCompositor}
import play.api.cache.Cache
import topfeed.server.security.AdminSecurityConstants
import play.api.Play.current
import scala.util.Random


object Admin extends Controller with InjectorProvider with HttpsOnlyActionCompositor
with AdminSecurityConstants {
  val gds = injector.instance[DatabaseService].gds
  val cypherExecutor = new ExecutionEngine(gds)
  
  def show = AdminAction {
    Ok(views.html.adminView())
  }
  
  def executeCypher = AdminAction {implicit request =>
    try {
      val query = request.body.asFormUrlEncoded.get("query").head
      inTransaction {
        val res = cypherExecutor.execute(query)
        Ok(views.html.adminView(Some(Left(res))))
      }
    } catch {
      case se: SyntaxException => Ok(views.html.adminView(Some(Right(se))))
      case _: Exception => BadRequest("Something went wrong!")
    }
  }
  
  def showLogin = httpsOnly {
	Action {
	  Ok(views.html.AdminLogin())
	}    
  }
  
  def processLogin = httpsOnly {
    Action {request =>
      (for(params <- request.body.asFormUrlEncoded;
          username <- params.get("username");
    	  password <- params.get("pass")
      ) yield (username, password)).collect {
        case (Seq("pouria"), Seq("Z31n0dd1n")) =>
          val sessionId = Random.alphanumeric.take(500).mkString
          Cache.set(admSessionCookieName, sessionId)
          (request.session.get(requestedUrlBeforeAdminLoginSessionName) match {
            
            case Some(url) => Ok(Html(s"""<a href="$url">Continue</a>"""))
            case None => Ok("Success")
          }).withCookies(Cookie(admSessionCookieName, sessionId, secure = true))
      } getOrElse {
        Redirect(routes.Admin.showLogin)
      }
    }
  }
  
  private def inTransaction[R](block: => R) = {
    val tx = gds.beginTx
    try {
      block
    } finally {
      tx.finish
    }
  }
}