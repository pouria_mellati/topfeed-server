package controllers

import play.api.mvc.Controller
import topfeed.server.security.AuthActionBuilder
import topfeed.server.config.di.InjectorProvider
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import topfeed.server.FeedService
import play.api.libs.json.Json
import topfeed.server.json.{UserFeedsWriter, BulkReadsReader, FavoriteFeedFormat, ActionJsonHelpers}
import topfeed.server.dataaccess.dao.NoSuchEntityError
import topfeed.server.Vote
import topfeed.server.FeedVotingException
import topfeed.server.domain.FavoriteFeed

object Feeds extends Controller with InjectorProvider with AuthActionBuilder
with UserFeedsWriter with BulkReadsReader with FavoriteFeedFormat with ActionJsonHelpers {
  val feedService = injector.instance[FeedService]
  
  def feedInfosByUsername = ApiAuthAction {username => implicit request =>
    Ok(Json toJson feedService.userFeedsByUsername(username))
  }
  
  def vote(channelUrl: String, feedUrl: String, voteStr: String) = ApiAuthAction {username => implicit request =>
    val vote = voteStr match {
      case "Up" => Vote.Up
      case "None" => Vote.None
      case "Down" => Vote.Down
    }
    
    try {
      feedService.vote(username, channelUrl, feedUrl, vote)
      Ok
    } catch {
      case e: NoSuchEntityError => BadRequest(s"No such entity:\n${e.getMessage}")
      case e: FeedVotingException => BadRequest("Attempted to vote on the same feed twice.")
    }
  }
  
  def favorite = ApiAuthAction {username => implicit request =>
    withJsonBodyAs[FavoriteFeed] { favFeed =>
      try {
        feedService.favorite(username, favFeed)
        Ok
      } catch {
      	case e: NoSuchEntityError => BadRequest(e.getMessage)
      }
    }
  }
  
  def unfavorite(feedUrl: String) = ApiAuthAction {username => implicit request =>
    try {
      feedService.unfavorite(username, feedUrl)
      Ok
    } catch {
      case e: NoSuchEntityError => BadRequest(e.getMessage)
    }
  }
  
  def getUserFavoriteFeeds = ApiAuthAction {username => implicit request =>
    Ok(Json.toJson(feedService.favoriteFeedsByUsername(username)))
  }
  
  def addFeedReads = ApiAuthAction {username => implicit request =>
    withJsonBodyAs[Map[String, Set[String]]] {bulkReads =>
      try {
        feedService.batchAddReads(username, bulkReads)
        Ok
      } catch {
        case e: NoSuchEntityError => BadRequest(e.getMessage)
      }
    }
  }
}