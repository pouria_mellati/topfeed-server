package topfeed.server.config.di

import com.google.inject.Guice
import org.neo4j.scala.SingletonEmbeddedGraphDatabaseServiceProvider

trait InjectorProvider {
  def injector = ProductionDiAndDatasourceConfiguration.injector 
  
  private object ProductionDiAndDatasourceConfiguration extends SingletonEmbeddedGraphDatabaseServiceProvider {
    sys.ShutdownHookThread{ds.gds.shutdown}
  
    def / = System.getProperty("file.separator")
    
    def topfeedServerHome =  System.getProperty("topfeed.home",
        System.getProperty("user.home") + / + "topfeedserver")
        
    def neo4jStoreDir = topfeedServerHome + / + "db"
    
    lazy val injector = Guice.createInjector(new DiBindingsModule(ds))
  }
}