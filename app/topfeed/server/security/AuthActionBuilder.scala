package topfeed.server.security

import play.api.mvc.{Security, Action, Result, Results, AnyContent, Request, RequestHeader}
import topfeed.server.domain.User
import topfeed.server.SessionService
import topfeed.server.config.di.InjectorProvider
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import play.api.mvc.BodyParsers
import play.api.libs.json.JsValue

// TODO: Rename to ApiAuthActionBuilder.
trait AuthActionBuilder extends InjectorProvider {
  private val sessionService = injector.instance[SessionService]
  
  private def userFromHeaders(header: RequestHeader): Option[String] =
    header.headers.get("Session-Id") flatMap {sessionService usernameBySessionId _}
  
  private def onUnauthorizedApiUse(header: RequestHeader) = Results.Unauthorized
  
  def ApiAuthAction(action: String => Request[AnyContent] => Result) =
    Security.Authenticated(userFromHeaders, onUnauthorizedApiUse) {username =>
      Action {request => action(username)(request)}
  	}
}