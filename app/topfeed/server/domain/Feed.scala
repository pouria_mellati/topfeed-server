package topfeed.server.domain

import topfeed.server.Vote
import com.github.nscala_time.time.Imports._

case class Feed(url: String,
    var numUpvotes: Int,
    var numDnvotes: Int,
    var numReads: Int,
    creationTimeStr: String = DateTime.now.toString())