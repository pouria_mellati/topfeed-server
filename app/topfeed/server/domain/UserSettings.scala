package topfeed.server.domain

import collection.mutable.Set

case class UserSettings(channels: Set[(FeedChannel, Rgb, FeedChannelPriority)])

case class Rgb(r: Double, g: Double, b: Double) {
  for(component <- List(r,g,b))
    if(component < 0 || component > 1)
      throw new IllegalArgumentException
}

object BuildRgb {
  def from256(r: Int, g: Int, b: Int) = Rgb(r / 256.0, g / 256.0, b / 256.0)
}

case class FeedChannelPriority(value: Double) {
  if(value < 0 || value > 1)
    throw new IllegalArgumentException("FeedChannelPriority value must be between 0 and 1.")
}