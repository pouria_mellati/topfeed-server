package topfeed.server.dataaccess.dao

import org.neo4j.scala.DatabaseService
import org.neo4j.scala.Neo4jWrapper
import org.neo4j.graphdb.Node
import scala.collection.JavaConverters._
import org.neo4j.graphdb.index.IndexHits
import scala.collection.convert.Wrappers.JIterableWrapper

class DaoMaker(val ds: DatabaseService) {
  self =>
    
  def makeForType[T <: AnyRef](nodeType: String, indexKeys: List[String], isConnectedToRefNode: Boolean = false) = new Dao[T] with Neo4jWrapper {
    val ds = self.ds
	private val index = ds.gds.index().forNodes(nodeType)
    
	val relationShipTypeToRefNode = nodeType
	
	def singleBy[ValT](indexKey: String, value: ValT): Node =
	  singleOptionBy[ValT](indexKey, value) match {
        case None => throw new NoSuchEntityError(s"$nodeType: with $indexKey = $value")
        case Some(node) => node
      }
	
    def singleOptionBy[ValT](indexKey: String, value: ValT): Option[Node] =
      Option(indexHitsBy(indexKey, value).getSingle)
    
    def singleCcBy[CC: Manifest, ValT](indexKey: String, value: ValT): CC =
      singleBy[ValT](indexKey, value).toCC[CC].get
      
    def singleCcOptionBy[CC: Manifest, ValT](indexKey: String, value: ValT): Option[CC] =
      singleOptionBy(indexKey, value) flatMap {_.toCC[CC]}
    
    def indexHitsBy[ValT](indexKey: String, value: ValT): IndexHits[Node] =
      if(indexKeys contains indexKey)
    	index.get(indexKey, value)
      else throw new IllegalArgumentException
      
    def add(node: Node)(implicit ds: DatabaseService): Node = {
	  if(isConnectedToRefNode)
	    getReferenceNode --> relationShipTypeToRefNode --> node
	  for(indexKey <- indexKeys)
		index.add(node, indexKey, node.getProperty(indexKey))
      node
	}
    
    def add(caseClassObj: T)(implicit ds: DatabaseService): Node = add(createNode(caseClassObj))
    
    def delete(node: Node) {
      if(isConnectedToRefNode)
        node.getRelationships(relationShipTypeToRefNode).asScala.foreach{_.delete}
      index.remove(node)
      node.delete
    }
    
    def deleteBy[ValT](indexKey: String, indexVal: ValT) {
      JIterableWrapper(indexHitsBy(indexKey, indexVal)) foreach {
        delete(_)
      }
    }
    
    def deleteIfPossible(node: Node) {
      if(isConnectedToRefNode)
        if(node.getRelationships.asScala.size <= 1) delete(node)
      else if(! node.hasRelationship) delete(node)
    }
  }
}

class NoSuchEntityError(msg: String) extends RuntimeException(msg)