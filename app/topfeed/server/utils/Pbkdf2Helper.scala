package topfeed.server.utils

import javax.crypto.{SecretKey, SecretKeyFactory}
import javax.crypto.spec.PBEKeySpec;
import java.security.SecureRandom;
import org.apache.commons.codec.binary.Base64;


// Based on an answer in http://stackoverflow.com/questions/2860943/suggestions-for-library-to-hash-passwords-in-java.
object Pbkdf2Password {
  private val iterations = 10 * 1024
  private val saltLen = 32
  private val desiredKeyLen = 256
  
  private val secureRandomInstance = SecureRandom.getInstance("SHA1PRNG") 

  def hashOf(password: String): String = {
    val saltBytes = new Array[Byte](saltLen)
    secureRandomInstance.nextBytes(saltBytes)	// Using generateSeed() took way too much time on the server.
    Base64.encodeBase64String(saltBytes) + "$" + hash(password, saltBytes)
  }

  def plaintextEqualsHashed(password: String, hashed: String): Boolean = {
    val saltAndPass = hashed.split("\\$")
    if (saltAndPass.length != 2)
      return false
    val hashOfInput = hash(password, Base64.decodeBase64(saltAndPass(0)))
    hashOfInput equals saltAndPass(1)
  }

  // using PBKDF2 from Sun, an alternative is https://github.com/wg/scrypt
  // cf. http://www.unlimitednovelty.com/2012/03/dont-use-bcrypt.html
  private def hash(password: String, salt: Array[Byte]): String = {
    if (password == null || password.length == 0)
      throw new IllegalArgumentException("Empty passwords are not supported.")
    val f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
    val secretKey = f.generateSecret(new PBEKeySpec(password.toCharArray, salt, iterations, desiredKeyLen))
    return Base64.encodeBase64String(secretKey.getEncoded)
  }
}
