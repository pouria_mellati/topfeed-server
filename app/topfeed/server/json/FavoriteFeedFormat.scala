package topfeed.server.json

import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError
import topfeed.server.domain.FavoriteFeed

trait FavoriteFeedFormat extends FavoriteFeedReader with FavoriteFeedWriter {
  implicit val favoriteFeedFormat = Format[FavoriteFeed](favoriteFeedReads, favoriteFeedWrites)
}

trait FavoriteFeedReader {
  import play.api.libs.json.Reads._
  
  implicit val favoriteFeedReads: Reads[FavoriteFeed] =
    (
      (__ \ "url").read[String] and
      (__ \ "channel").read[String] and
      (__ \ "title").read[String] and
      (__ \ "description").read[String] and
      (__ \ "time").read[String]
    )(FavoriteFeed)
}

trait FavoriteFeedWriter {
  import play.api.libs.json.Writes._
  
  implicit val favoriteFeedWrites: Writes[FavoriteFeed] =
    (
      (__ \ "url").write[String] and
      (__ \ "channel").write[String] and
      (__ \ "title").write[String] and
      (__ \ "description").write[String] and
      (__ \ "time").write[String]
    ) (unlift(FavoriteFeed.unapply))
}