package topfeed.server.json

import topfeed.server.domain.{UserFeed, FeedChannel}
import play.api.libs.json._
import play.api.libs.json.Writes._
import play.api.libs.functional.syntax._
import topfeed.server.Vote


trait UserFeedsWriter extends UserFeedWriter {
  implicit val userFeedsWrites: Writes[Map[FeedChannel, Iterable[UserFeed]]] =
    traversableWrites[(FeedChannel, Iterable[UserFeed])] (
      (
        (__ \ "channel").write[String] and
        (__ \ "feeds").write(traversableWrites[UserFeed](userFeedWrites))
      ) ((tup: (FeedChannel, Iterable[UserFeed])) => (tup._1.url, tup._2))
    )
}