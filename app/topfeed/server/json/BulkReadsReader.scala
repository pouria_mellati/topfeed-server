package topfeed.server.json

import play.api.libs.json._
import play.api.libs.functional.syntax._

/**
 * Converts a json object (describing batch reads) to a Map from channel urls to sets of feed urls.
 */
trait BulkReadsReader {
  import play.api.libs.json.Reads._
  
  implicit val bulkReadsReads: Reads[Map[String, Set[String]]] =
    __.read(list[(String, Set[String])] (
        (
          (__ \ "channel").read[String] and
          (__ \ "feeds").read(set[String])
        )
        (_ -> _)
      )
    ) map {_.toMap}
}