package topfeed.server.json

import play.api.mvc.{Result, Controller, Request, AnyContent}
import play.api.libs.json.JsValue
import play.api.libs.json.Reads

trait ActionJsonHelpers {
  this: Controller =>
  
  def withJsonBody(action: JsValue => Result)(implicit request: Request[AnyContent]): Result = {
    request.body.asJson.fold[Result] {
      BadRequest("Could not interpret request payload as json.")
    }{
      action(_)
    }
  }
  
  def withJsonBodyAs[T](action: T => Result)(implicit request: Request[AnyContent], reads: Reads[T]): Result =
    try {
      withJsonBody {
        _.validate[T].fold[Result] (
          errors => BadRequest(errors.toString),
          (obj: T) => action(obj)
        )
      }
    } catch {
      case e: IllegalArgumentException =>
        BadRequest("Could not convert the supplied json to the expected type: Illegal Argument.")
      case e: Exception =>
        BadRequest("Could not convert the supplied json to the expected type.")
    }
}