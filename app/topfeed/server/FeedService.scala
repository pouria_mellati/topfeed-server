package topfeed.server

import scala.collection.JavaConverters._
import topfeed.server.domain.{Feed, User, FavoriteFeed, UserFeed, FeedChannel}
import org.neo4j.scala.DatabaseService
import javax.inject.Inject
import org.neo4j.scala.Neo4jWrapper
import topfeed.server.dataaccess.UserRepository
import topfeed.server.dataaccess.dao.Dao
import org.neo4j.graphdb.Node

class FeedService @Inject() (
    val ds: DatabaseService,
    userRepo: UserRepository,
    channelDao: Dao[FeedChannel],
    feedDao: Dao[Feed],
    favFeedDao: Dao[FavoriteFeed]
) extends Neo4jWrapper {
  def vote(username: String, channelUrl: String, feedUrl: String, vote: Vote.Value) {
    withTx {implicit ds =>
      val userNode = userRepo userNodeByUsername username
      val channelNode = channelDao.singleBy("url", channelUrl)
      val feedNode = returnExistingFeedOrCreateNew(channelNode, feedUrl)
      
      val relationshipType = vote match {
        case Vote.None => removeVote(userNode, feedNode)
        case Vote.Up   => upvote(userNode, feedNode)
        case Vote.Down => dnvote(userNode, feedNode)
      }    
    }
  }
  
  def userFeedsByUsername(username: String): Map[FeedChannel, Iterable[UserFeed]] = {
    val userNode = userRepo.userNodeByUsername(username)
    val upvotedFeedNodes = Set.empty[Node] ++ userNode.getRelationships(Relationships.HasUpvoted).asScala.map{_.getEndNode}
    val dnvotedFeedNodes = Set.empty[Node] ++ userNode.getRelationships(Relationships.HasDnvoted).asScala.map{_.getEndNode}
    val readFeedNodes 	 = Set.empty[Node] ++ userNode.getRelationships(Relationships.HasRead).asScala.map{_.getEndNode}
    
    // TODO: This is stupid. Move all the relationship type strings of a node type to a central place.
    userNode.getRelationships(userRepo.RelationshipType.follows).asScala.map{followsRel =>
      val channelNode = followsRel.getEndNode
      
      channelNode.toCC[FeedChannel].get ->
      channelNode.getRelationships("HAS_FEED").asScala.map{hasFeedRel =>	// TODO: Fix bare "HAS_FEED".
        val feedNode = hasFeedRel.getEndNode
        val feed = feedNode.toCC[Feed].get
        
        val userVote = if(upvotedFeedNodes contains feedNode) Vote.Up
        	else if (dnvotedFeedNodes contains feedNode) Vote.Down
        	else Vote.None
        
        UserFeed(
          url = feed.url,
          numUpvotes = feed.numUpvotes,
          numDnvotes = feed.numDnvotes,
          numReads = feed.numReads,
          feedCreationTimeStr = feed.creationTimeStr,
          vote = userVote,
          isReadByUser = readFeedNodes contains feedNode
        )
      }
    }.toMap
  }
  
  private def removeVote(userNode: Node, feedNode: Node) {
    removeUpvoteIfExists(userNode, feedNode)
    removeDnvoteIfExists(userNode, feedNode)
  }
  
  private def upvote(userNode: Node, feedNode: Node) {
    if(! getUpvotes(userNode, feedNode).isEmpty)
      throw new FeedVotingException("A user cannot upvote an item more than once.")
    removeDnvoteIfExists(userNode, feedNode)
    userNode.createRelationshipTo(feedNode, Relationships.HasUpvoted)
    updateFeedAndSave(feedNode){_.numUpvotes += 1}
  }
  
  private def dnvote(userNode: Node, feedNode: Node) {
    if(! getDnvotes(userNode, feedNode).isEmpty)
      throw new FeedVotingException("A user cannot downvote an item more than once.")
    removeUpvoteIfExists(userNode, feedNode)
    userNode.createRelationshipTo(feedNode, Relationships.HasDnvoted)
    updateFeedAndSave(feedNode){_.numDnvotes += 1}
  }
  
  private def getUpvotes(userNode: Node, feedNode: Node) =
    feedNode.getRelationships(Relationships.HasUpvoted).asScala.filter{_.getStartNode == userNode}
  
  private def getDnvotes(userNode: Node, feedNode: Node) =
    feedNode.getRelationships(Relationships.HasDnvoted).asScala.filter{_.getStartNode == userNode}
  
  private def removeUpvoteIfExists(userNode: Node, feedNode: Node) {
    getUpvotes(userNode, feedNode) foreach {r =>
      r.delete
      updateFeedAndSave(feedNode){_.numUpvotes -= 1}
    }
  }
  
  private def removeDnvoteIfExists(userNode: Node, feedNode: Node) {
    getDnvotes(userNode, feedNode) foreach {r =>
      r.delete
      updateFeedAndSave(feedNode){_.numDnvotes -= 1}
    }
  }
  
  // The reads param maps from channel urls to sets of feed urls. 
  def batchAddReads(username: String, reads: Map[String, Set[String]]) {
    withTx {implicit ds =>
      val userNode = userRepo.userNodeByUsername(username)
      val alreadyReadFeedUrls= userNode.getRelationships(Relationships.HasRead).asScala.map{_.getEndNode.toCC[Feed].get.url}
      
      for((channelUrl, feedUrlsToSetAsRead) <- reads) {
        val channelNode = channelDao.singleBy("url", channelUrl)
        for(feedUrlToSetAsRead <- (feedUrlsToSetAsRead -- alreadyReadFeedUrls)) {
          val feedNode = returnExistingFeedOrCreateNew(channelNode, feedUrlToSetAsRead)
          userNode --> Relationships.HasRead --> feedNode
          updateFeedAndSave(feedNode) {_.numReads += 1}
        }
      }
    }
  }
  
  def hasUserReadFeed(username: String, feedUrl: String): Boolean = {
	val userNode = userRepo.userNodeByUsername(username)
    val feedNode = feedDao.singleBy("url", feedUrl)
    feedNode.getRelationships(Relationships.HasRead).asScala.exists{_.getStartNode == userNode}
  }
  
  private def updateFeedAndSave(feedNode: Node)(updateFunc: Feed => Unit) {
    val feed = feedNode.toCC[Feed].get
    updateFunc(feed)
    Neo4jWrapper.serialize[Node](feed, feedNode)
  }
  
  def getUserVote(username: String, feedUrl: String): Vote.Value = {
    val feedNode = feedDao.singleBy("url", feedUrl)
    val userNode = userRepo.userNodeByUsername(username)
    if(feedNode.getRelationships(Relationships.HasUpvoted).asScala.exists{_.getStartNode == userNode})
      Vote.Up
    else if(feedNode.getRelationships(Relationships.HasDnvoted).asScala.exists{_.getStartNode == userNode})
      Vote.Down
    else
      Vote.None
  }
    
  def feedsByChannelUrl(channelUrl: String): Iterable[Feed] = {
    val channelNode = channelDao.singleBy("url", channelUrl)
    channelNode.getRelationships("HAS_FEED").asScala map {_.getEndNode().toCC[Feed].get}
  }
  
  def favorite(username: String, favFeed: FavoriteFeed) {
    import com.github.nscala_time.time.Imports._
    
    favFeed.creationDateStr = DateTime.now.toString() 
    favFeed.title = favFeed.title.take(200) + "..."
    favFeed.description = favFeed.description.take(400) + "..."
    
    withTx {implicit ds =>
      val userNode = userRepo.userNodeByUsername(username)
      val favFeedNode = favFeedDao.singleOptionBy("url", favFeed.url).getOrElse {favFeedDao add favFeed}
      if(! userNode.getRelationships(Relationships.HasFavorited).asScala.exists(_.getEndNode == favFeedNode))
    	  userNode --> Relationships.HasFavorited --> favFeedNode
    }
  }
  
  def unfavorite(username: String, feedUrl: String) {
    withTx {implicit ds =>
      val favFeedNode = favFeedDao.singleBy("url", feedUrl)
      val userNode = userRepo.userNodeByUsername(username)
      userNode.getRelationships(Relationships.HasFavorited).asScala.find{_.getEndNode == favFeedNode} match {
        case Some(r) => r.delete
        case _ => Unit
      }
    }
  }
  
  def favoriteFeedsByUsername(username: String): Iterable[FavoriteFeed] = {
    userRepo.userNodeByUsername(username).getRelationships(Relationships.HasFavorited).asScala.map {
      _.getEndNode.toCC[FavoriteFeed].get
    }
  }
  
  private def returnExistingFeedOrCreateNew(channelNode: Node, feedUrl: String)(implicit ds: DatabaseService): Node =
    channelNode.getRelationships("HAS_FEED").asScala find {		// TODO: Stop using bare string for relationship type.
      _.getEndNode().toCC[Feed].get.url == feedUrl
    } match {
      case Some(rel) => rel.getEndNode()
      case None =>
        val feedNode = feedDao.singleOptionBy("url", feedUrl) match {
          case None => feedDao.add(Feed(feedUrl, 0, 0, 0))
          case Some(existing) => existing
        }
        channelNode --> "HAS_FEED" --> feedNode
        feedNode
    }
    
    private object Relationships {
      val HasUpvoted = "HAS_UPVOTED"
      val HasDnvoted = "HAS_DNVOTED"
      val HasRead    = "HAS_READ"
      val HasFavorited = "HAS_FAVORITED"
    }
    
}

object Vote extends Enumeration {
  val Up, Down, None = Value
}

class FeedVotingException(msg: String) extends RuntimeException(msg)