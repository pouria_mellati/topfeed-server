package topfeed.server.dataaccess.dao

import org.specs2.mutable._
import util.ImpermanentGraphDatabaseServiceProviderScope
import org.neo4j.scala.Neo4jWrapper

case class DomainType(id: String, value: Int)

class DaoMakerSpec extends Specification {
  
  trait Environment extends ImpermanentGraphDatabaseServiceProviderScope with Neo4jWrapper with DaoMakerProvider {
    val madeDao = daoMaker.makeForType[DomainType]("DomainType", List("id"), isConnectedToRefNode = true)
  }
  
  val domainObj = DomainType("id1", 12)
  
  "A Dao made by the DaoMaker" should {
    "Properly add, retrieve (using singleOptionBy) and delete an object" in new Environment {
      withTx {implicit ds =>
        madeDao.add(domainObj)
        
        val retrievedOption = madeDao.singleOptionBy("id", domainObj.id)
        retrievedOption must beSome.which(_.toCC[DomainType] must beSome(domainObj))
        
        madeDao.delete(retrievedOption.get)
        madeDao.singleOptionBy("id", domainObj.id) must beNone
      }
    }
    
    "The deleteIfPossible method should only delete if there are no relationships" in new Environment {
      withTx {implicit ds =>
        val node = madeDao.add(domainObj)
        val relationship = node --> "someRelationship" --> node <()
        
        madeDao.deleteIfPossible(node)
        
        madeDao.singleOptionBy("id", domainObj.id) must beSome
        
        relationship.delete()
        madeDao.deleteIfPossible(node)
        
        madeDao.singleOptionBy("id", domainObj.id) must beNone
      }
    }
  }
}