package topfeed.server.dataaccess

import org.specs2.mutable._
import util.ImpermanentGraphDatabaseServiceProviderScope
import topfeed.server.domain.User
import topfeed.server.dataaccess.dao.{DaoMakerProvider, UserDaoProvider, FeedChannelDaoProvider}
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import util.TestingInjectorProviderScope

// TODO: Rename to reflect this being an integration spec.
class UserRepositorySpec extends Specification {
  def makeUser = User("username", "passss")
  
  trait Environment extends TestingInjectorProviderScope {
    val userRepo = injector.instance[UserRepository]
  }
  
  "UserRepository" should {
    "Properly add a user and retrieve them by username" in new Environment {
      val user = makeUser
      userRepo.withTx {implicit ds =>
        userRepo.addUser(user)
      }
      
      userRepo.withTx {implicit ds =>
        userRepo.userByUsernameOption(user.username)
      } must beSome[User]
    }
  }
}