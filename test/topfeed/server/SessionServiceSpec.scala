package topfeed.server

import org.specs2.mutable._
import org.specs2.specification.Scope
import topfeed.server.dataaccess.UserRepository
import org.specs2.mock.Mockito
import topfeed.server.domain.User
import util.ImpermanentGraphDatabaseServiceProviderScope
import topfeed.server.dataaccess.dao._
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import util.TestingInjectorProviderScope
import topfeed.server.utils.Pbkdf2Password

class SessionServiceSpec extends Specification with Mockito {
  trait Environment extends TestingInjectorProviderScope {
    val userRepo = injector.instance[UserRepository]
    val sessionService = injector.instance[SessionService]
  }
  
  val password = "password"
  val passHash = Pbkdf2Password hashOf password
  val user = User("username", passHash)
  
  "SessionService" should {
    "Properly login an existing user, logout a logged-in user and retrieve a logged-in user's username by session id" in new Environment{
      userRepo.withTx {implicit ds => userRepo.addUser(user)}
  
      val sessionOpt = sessionService.login(user.username, password)
      sessionOpt must beSome[String]
      sessionService.usernameBySessionId(sessionOpt.get) must beSome.which{_ must equalTo(user.username)}
      
      sessionService.logout(user.username)
      sessionService.usernameBySessionId(sessionOpt.get) must beNone
    }
    
    "Not login a non-existant user" in new Environment {
      sessionService.login("somenonexistantusername", "somepass") must beNone
    }
    
    "Not login with the wrong password" in new Environment {
      userRepo.withTx {implicit ds => userRepo.addUser(user)}
      
      sessionService.login(user.username, "wrongpass") must beNone
    }
    
    "Destroy the previous session on a new log-in" in new Environment {
      userRepo.withTx {implicit ds => userRepo.addUser(user)}
      val firstSessionId  = sessionService.login(user.username, password).get
      
      sessionService.login(user.username, password) must beSome
      sessionService.usernameBySessionId(firstSessionId) must beNone
    }
  }
}