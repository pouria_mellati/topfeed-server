package topfeed.server.json

import play.api.libs.json._
import topfeed.server.domain.{UserSettings, FeedChannel, Rgb, FeedChannelPriority}
import scala.collection.mutable.Set
import org.specs2.mutable._
import org.specs2.specification.Scope

class UserSettingsFormatSpec extends Specification with UserSettingsFormat {
  val (channel1, channel2) = (FeedChannel("http://url1)"), FeedChannel("http://url2)"))
  val (color1, color2) = (Rgb(1,0,1), Rgb(1,1,1))
  val (priority1, priority2) = (FeedChannelPriority(1), FeedChannelPriority(0.2)) 
  
  val userSettings = UserSettings(channels = Set(
      (channel1, color1, priority1),
      (channel2, color2, priority2)
  ))
  
  val userSettingsJs = Json.obj (
    "channels" -> Json.arr(
      Json.obj (
        "url" -> channel1.url,
        "color" -> Json.obj (
          "r" -> color1.r,
          "g" -> color1.g,
          "b" -> color1.b),
        "priority" -> priority1.value
      ),
      Json.obj (
        "url" -> channel2.url,
        "color" -> Json.obj (
          "r" -> color2.r,
          "g" -> color2.g,
          "b" -> color2.b),
        "priority" -> priority2.value
      )
    )
  )
  
  "json.UserSettingsFormat" should {
    "Properly serialize and deserialize UserSettings" in {
      Json.toJson(userSettings) must equalTo(userSettingsJs)
      Json.fromJson(userSettingsJs).get must equalTo(userSettings)
    }
  }
}