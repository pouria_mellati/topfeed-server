package topfeed.server.json

import play.api.libs.json._
import org.specs2.mutable._
import org.specs2.specification.Scope
import topfeed.server.domain.UserFeed
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import topfeed.server.Vote
import com.github.nscala_time.time.Imports._

class UserFeedWriterSpec extends Specification {
  trait Environment extends Scope with UserFeedWriter {
    val userFeed = UserFeed("http://url", isReadByUser = true, vote = Vote.Up, numUpvotes = 2,
        numDnvotes = 1, numReads = 120, feedCreationTimeStr = DateTime.now.toString())
    
    val userFeedJson = Json.obj(
	  "url" -> userFeed.url,
	  "creationTime" -> userFeed.feedCreationTimeStr,
	  "upVotes" -> userFeed.numUpvotes,
	  "dnVotes" -> userFeed.numDnvotes,
	  "reads"   -> userFeed.numReads,
	  "userVote" -> (if(userFeed.vote == Vote.Up) 1 else if(userFeed.vote == Vote.Down) -1 else 0),
	  "hasUserRead" -> userFeed.isReadByUser
	)
  }
  
  "UserFeedWriter (to json)" should {
    "Properly serialize a UserFeed into json" in new Environment {
      Json.toJson(userFeed) must_== userFeedJson
    }
  }
}