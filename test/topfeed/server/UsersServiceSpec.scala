package topfeed.server

import collection.mutable.Set
import org.specs2.mutable._
import org.specs2.mock.Mockito
import topfeed.server.domain.{User, UserSettings, FeedChannel, Rgb, FeedChannelPriority}
import topfeed.server.dataaccess.UserRepository
import org.specs2.specification.Scope
import org.neo4j.scala.DatabaseService
import util.ImpermanentGraphDatabaseServiceProviderScope
import topfeed.server.dataaccess.dao.{DaoMaker, DaoMakerProvider, UserDaoProvider, FeedChannelDaoProvider}
import topfeed.server.dataaccess.dao.NoSuchEntityError

object helper {
  def makeUser = User(username = "Someusername", hashedPass = "somePassHash")
  def makeUserSettings = UserSettings(channels = Set(
      (FeedChannel("http://url1"), Rgb(1,0,0), FeedChannelPriority(.4)),
      (FeedChannel("http://url2"), Rgb(0,1,0), FeedChannelPriority(.5))
  ))
}

class UsersServiceSpec extends Specification with Mockito {
  trait Environment extends Scope {
	val userRepo = mock[UserRepository]
	val usersService = new UsersService(userRepo)
    implicit val mockDatabaseServiceForTransactions = mock[DatabaseService]
    userRepo.withTx(any[DatabaseService => Any]) answers { operation =>
      operation.asInstanceOf[DatabaseService => Any].apply(mockDatabaseServiceForTransactions)}
  }
  
  "UsersService (UnitTest)" should {
    "Successfully add a user with an unused username" in new Environment {
      val user = helper.makeUser
      userRepo.userByUsernameOption(user.username) returns None
      
      usersService.addUser(user)
      
      there was one(userRepo).addUser(user)
    }
    
    "Fail to add a user with an already in-use username" in new Environment {
      val user = helper.makeUser
      userRepo.userByUsernameOption(user.username) returns Some(mock[User])
      
      usersService.addUser(user) must throwA[UsernameIsTakenException]
    }
  }
}

class UsersServiceIntegrationSpec extends Specification {
  import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
  import util.TestingInjectorProviderScope

  trait Environment extends TestingInjectorProviderScope {
    val usersService = injector.instance[UsersService]
  }
  
  "UsersService (Integration Test)" should {
    "Set and retrieve an existing user's settings" in new Environment {
      val user = helper.makeUser
      usersService.addUser(user)
      val userSettings = helper.makeUserSettings
      
      usersService.setUserSettings(user.username, userSettings)
      usersService.userSettingsByUsername(user.username) must equalTo(userSettings)
    }
    
    "Fail to set or retrieve settings for a non-existant user" in new Environment {
      usersService.userSettingsByUsername("nonexistantUser") must throwA[NoSuchEntityError]
      usersService.setUserSettings("nonexistantUser", helper.makeUserSettings) must throwA[NoSuchEntityError]
    }
  }
}
