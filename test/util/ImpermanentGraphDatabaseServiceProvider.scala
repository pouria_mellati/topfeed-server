package util

import org.neo4j.scala.DatabaseServiceImpl
import org.neo4j.test.TestGraphDatabaseFactory
import org.neo4j.scala.GraphDatabaseServiceProvider

trait ImpermanentGraphDatabaseServiceProvider extends GraphDatabaseServiceProvider {
  val ds = new DatabaseServiceImpl(new TestGraphDatabaseFactory().newImpermanentDatabase())
}